from tasks import add

result = add.delay(4, 4)

result.ready()

# run app
# celery -A tasks worker --loglevel=INFO

# check if config is ok
# python -m celeryconfig

# set rate limit from command line:
# celery -A tasks control rate_limit tasks.add 10/m
